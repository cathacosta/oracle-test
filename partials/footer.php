		<footer class="row text-center">
			<!-- <div class="row text-center"> -->

				<div class="col-md-12 logo-wrapper">
					<a href="https://www.oracle.com" target="_blank">
						<div class="div-logo">
							<svg id="logo-oracle" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" viewBox="0 0 500 70"><path fill="#fff" d="M490 11.9V9.8h.4c.7 0 1.1 0 1.4.1.4.2.6.5.6.9 0 .3-.1.5-.2.6-.3.4-.8.5-1.3.5h-.9zm-1.6-3.4V16h1.5v-2.9h1.1l1.6 2.9h1.7l-1.8-3.1c.2-.1.3-.1.4-.2.7-.4.9-1 1-1.3.1-.3.1-.5.1-.6 0-.4-.1-1.7-1.5-2.1-.5-.2-1-.2-2-.2h-2.1zm-4.3 4c0-3.7 3.1-6.7 6.9-6.7 3.8 0 6.9 3 6.9 6.7s-3.1 6.7-6.9 6.7c-3.8-.1-6.9-3-6.9-6.7m6.9 5.2c3 0 5.4-2.3 5.4-5.2 0-2.9-2.4-5.2-5.4-5.2s-5.4 2.3-5.4 5.2c0 2.9 2.4 5.2 5.4 5.2m-53.3 37.2c-9.1 0-16.8-5.9-19.1-13.9H469l7-10.4h-57.5c2.3-8.1 10-14 19.1-14h34.7l7-10.4h-42.4c-16.9 0-30.7 13.3-30.7 29.6 0 16.4 13.8 29.6 30.7 29.6h36.4l6.9-10.4h-42.5zM293.2 65.4c-17 0-30.7-13.3-30.7-29.6 0-16.4 13.8-29.6 30.7-29.6h42.4l-7 10.4H294c-11 0-19.9 8.6-19.9 19.2S283 55 294 55h42.6l-6.9 10.4h-36.5zM68.9 54.9c11 0 19.9-8.6 19.9-19.2s-8.9-19.2-19.9-19.2H34.8c-11 0-19.9 8.6-19.9 19.2s8.9 19.2 19.9 19.2h34.1zM34 65.4c-17 0-30.7-13.3-30.7-29.6C3.3 19.4 17 6.2 34 6.2h35.7c17 0 30.7 13.3 30.7 29.6 0 16.4-13.7 29.6-30.7 29.6H34zm124.4-19.2c11.5 0 20.8-9 20.8-20s-9.3-20-20.8-20h-51.7v59.2h11.8V16.6h39.1c5.5 0 9.9 4.3 9.9 9.6s-4.4 9.6-9.9 9.6h-33.3l35.2 29.6h17.2L153 46.2h5.4zm195.4 8.7V6.2H342v53.6c0 1.5.6 2.9 1.7 4 1.2 1.1 2.7 1.7 4.3 1.7h53.9l6.9-10.4h-55zM209.2 44.5h31.5L224 18.6l-30.6 46.8h-13.9l37.2-56.2c1.6-2.3 4.3-3.7 7.3-3.7 2.9 0 5.6 1.3 7.2 3.6l37.3 56.3h-13.9l-6.5-10.5h-31.9l-7-10.4z"></path></svg>
						</div>
						<p class="logo-description">Integrated Cloud Applications & Platform Services</p>
					</a>
				</div>

				<div class="col-md-12 div-social ">
					<span><a target="_blank" href="https://www.facebook.com/Oracle"><i class="fab fa-2x fa-facebook-square"></i></a></span>
					<span><a target="_blank" href="https://twitter.com/Oracle"><i class="fab fa-2x fa-twitter-square"></i></a></span>
					<span><a target="_blank" href="https://plus.google.com/u/0/+Oracle/posts"><i class="fab fa-2x fa-google-plus-square"></i></a></span>
					<span><a target="_blank" href="http://www.oracle.com/us/social-media/linkedin/index.html"><i class="fab fa-2x fa-linkedin"></i></a></span>
					<span><a target="_blank" href="http://www.youtube.com/oracle/"><i class="fab fa-2x fa-youtube-square"></i></a></span>
					<span><a target="_blank" href="https://blogs.oracle.com/rss"><i class="fas fa-2x fa-rss-square"></i></a></span>
				</div>
			<!-- </div> -->

		</footer>
    </div>
    <script src="js/vendor/jquery/jquery-3.2.1.js"></script>
    <script src="js/vendor/bootstrap/bootstrap.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
