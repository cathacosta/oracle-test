<?php 
	$page_slug = $_GET['slug'];
	include('app/article.php');
	include('partials/header.php');
?>
	<!-- Sub Menu -->
	<nav class="sub-menu fixed-top" aria-label="breadcrumb">
      <ol class="oracle-container breadcrumb">
        <li class="breadcrumb-item"><a href="/">News</a></li>
        <li class="breadcrumb-item"><a href="news?category=<?= $page_article['category']; ?>"><?= $page_article['category']; ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?= $page_article['title']; ?></li>
      </ol>
  	</nav>
  	<!-- Article Block -->
	<div class="oracle-container body-container" id="article_block">
		<div class="row article_header">
			<div class="col-md-12">
				<p class="lblCategory"><i class="fas fa-bullseye"></i> <?= $page_article['category']; ?></p>
				<h1 class="article_title"><?= $page_article['title']; ?></h1>
				<p class="article_create"><em>By <span><?= $page_article['creator']; ?> </span></em><span class="text-grey"> | <?= $page_article['pubDate']; ?></span></p>
			</div>
		</div>

		<div class="row article_body">
			<div class="col-md-8 col-sm-12 article-content">
				<img src="<?= $page_article['enclosure']; ?>" class="article-enclosure">
				<?= $page_article['description']; ?>
			</div>

			<div class="col-md-4 col-sm-12 recent-content">
				<!-- Recent News Block -->
				<div class="row desktop-view" id="recent_block">
					<h6 class="block-header">Recent News</h6>
					<div class="col-md-12 v-news">
						<?php
							$i = 0; 
							foreach ($items as $item) : 
								if($page_slug !== $item['slug'] ) {
						?>
									<div class="row v-row">
										<div class="col-md-12">
											<a href="article?slug=<?= $item['slug']; ?>">
												<div class="row">
													<div class="col-8">
														<p class="title"><?= $item['title']; ?></p>
														<p class="description">
															<?php 
																$desc = strip_tags($item['description']); 
																if(strlen($desc) <= 80 ) 
																	echo $desc;
																else 
																	echo substr($desc, 0, 80) . '...';
															?>
														</p>
													</div>
													<div class="col-4">
														<div class="sm-img-news" style="background-image: url(<?= $item['enclosure']; ?>)"></div>
													</div>
												</div>
											</a>
										</div>
									</div>
						<?php
									$i++;
									if($i >= 5) break; 
								}
							endforeach; 
						?>
					</div>
				
				</div>

			</div>

		</div>

		<div class="row category_lists desktop-view-sm mobile-view">
			<div class="col-md-12">
				<h4>Recent News</h4>
			</div>
			<?php
				$i = 0; 
				foreach ($items as $item) : 
					if($page_slug !== $item['slug'] ) {
			?>
						<div class="col-md-4 col-sm-6">
							<a href="article?slug=<?= $item['slug']; ?>">
								<div class="news-wrap">
									<div class="img-news" style="background-image: url(<?= $item['enclosure']; ?>)"></div>
									<div class="img-overlay">
										<p class="category"><i class="fas fa-bullseye"></i> <?= $item['category']; ?></p>
										<h5 class="title"><?= $item['title']; ?></h5>
										<p class="details">By: <span class="creator"><?= $item['creator']; ?></span> | <?= $item['pubDate']; ?></p>
										<p class="description">
											<?php 
												$desc = strip_tags($item['description']); 
												if(strlen($desc) <= 85 ) 
													echo $desc;
												else 
													echo substr($desc, 0, 85) . '...';
											?>
										</p>
										<div class="float-right">
											<button class="btn btn-sm btn-default btn-red">Read More</button>
										</div>
									</div>
								</div>
							</a>
						</div>
			<?php
					$i++;
					if($i >= 6) break; 
					}
				endforeach; 
			?>
		</div>

		<?php if(count($page_items) > 1 ) { ?>
			<div class="row category_lists">
				<div class="col-md-12">
					<h4>Related News</h4>
				</div>
				<?php 
						$i = 0;
						foreach ($page_items as $key => $value) : 
						$article_slug = $createSlug->slug($value->title);
						if($page_slug !== $article_slug ) {
					?>

					<div class="col-md-4 col-sm-6">
						<a href="article?slug=<?= $article_slug; ?>">
							<div class="news-wrap">
								<div class="img-news" style="background-image: url(<?= $value->enclosure['url']; ?>)"></div>
								<div class="img-overlay">
									<p class="category"><i class="fas fa-bullseye"></i> <?= $value->category; ?></p>
									<h5 class="title"><?= $value->title; ?></h5>
									<p class="details">By: <span class="creator"><?= $value->creator; ?></span> | <?= $value->pubDate; ?></p>
									<p class="description">
										<?php 
											$desc = strip_tags($value->description); 
											if(strlen($desc) <= 160 ) 
												echo $desc;
											else 
												echo substr($desc, 0, 160) . '...';
										?>
									</p>
									<div class="float-right">
										<button class="btn btn-sm btn-default btn-red">Read More</button>
									</div>
								</div>
							</div>
						</a>
					</div>
				<?php 
						$i++;
						if($i >= 3) break; 
						}
					endforeach; 
					?>
			</div>
		<?php } ?>



	</div>

<?php include('partials/footer.php'); ?>
