
<?php include('landing/header.php'); ?>

<!-- Categories Block -->
<div class="oracle-container" id="categories_block">
	<div class="row category_header">
		<div class="col-md-12 text-center">
			<!-- <div class="float-right"> -->
				<a href="news?category=All" class="btn btn-default btn-red-black">All Recent News</a>
			<!-- </div> -->
		</div>
	</div>
	<div class="row category_lists">
		
		<?php foreach ($category_items as $key => $value) : ?>
			<?php if($key == 0) { ?> 
				<div class="col-md-8 col-sm-6">
					<a href="article?slug=<?= $value['slug']; ?>">
						<div class="news-wrap">
							<div class="img-news" style="background-image: url(<?= $value['enclosure']; ?>)"></div>
							<div class="img-overlay">
								<p class="category"><i class="fas fa-bullseye"></i> <?= $value['category']; ?></p>
								<h5 class="title"><?= $value['title']; ?></h5>
								<p class="details">By: <span class="creator"><?= $value['creator']; ?></span> | <?= $value['pubDate']; ?></p>
								<p class="description desktop-view">
									<?php 
										$desc = strip_tags($value['description']); 
										if(strlen($desc) <= 500 ) 
											echo $desc;
										else 
											echo substr($desc, 0, 500) . '...';
									?>
								</p>

								<p class="description desktop-view-sm">
									<?php 
										$desc = strip_tags($value['description']); 
										if(strlen($desc) <= 350 ) 
											echo $desc;
										else 
											echo substr($desc, 0, 350) . '...';
									?>
								</p>

								<p class="description mobile-view">
									<?php 
										$desc = strip_tags($value['description']); 
										if(strlen($desc) <= 150 ) 
											echo $desc;
										else 
											echo substr($desc, 0, 150) . '...';
									?>
								</p>
								<div class="float-right">
									<button class="btn btn-sm btn-default btn-red">Read More</button>
								</div>
							</div>
						</div>
					</a>
				</div>
			<?php }else{ ?>
				<div class="col-md-4 col-sm-6">
					<a href="article?slug=<?= $value['slug']; ?>">
						<div class="news-wrap">
							<div class="img-news" style="background-image: url(<?= $value['enclosure']; ?>)"></div>
							<div class="img-overlay">
								<p class="category"><i class="fas fa-bullseye"></i> <?= $value['category']; ?></p>
								<h5 class="title"><?= $value['title']; ?></h5>
								<p class="details">By: <span class="creator"><?= $value['creator']; ?></span> | <?= $value['pubDate']; ?></p>
								<p class="description">
									<?php 
										$desc = strip_tags($value['description']); 
										if(strlen($desc) <= 150 ) 
											echo $desc;
										else 
											echo substr($desc, 0, 150) . '...';
									?>
								</p>
								<div class="float-right">
									<button class="btn btn-sm btn-default btn-red">Read More</button>
								</div>
							</div>
						</div>
					</a>
				</div>
			<?php } ?>
		<?php endforeach; ?>
	</div>
	
</div>


<?php include('partials/footer.php'); ?>

