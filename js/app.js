var app = {
	init: function() {
		var self = this;
		self.navInit();
		self.fetchNews();
	},

	navInit: function() {
		var start = function() {
			$(document).on('scroll', scrollNav);
		},
		scrollNav = function() {
			var $nav = $(".fixed-top");
    		$nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
		};
		start();
	},

	fetchNews: function() {
		var start = function() {
			$('.btn-sort').on('click', sortNews);
			$('#news_card_template').hide();
		},
		sortNews = function() {
			var url = 'app/sort.php',
				param = {
					sort : $('.select-sort').val(),
					category : $('.page_category').val(), 
				};
			$.get(url, param).done(function(data) {
				var wrapper = $('.news_lists');
				wrapper.find('.news_card').remove();
				$.each(data, function(i, v) {
					var template = $('#news_card_template').clone();
						template.removeAttr('id');
						template.find('.news-link').text(v.slug);
						template.find('.title').text(v.title);
						template.find('.lblCategory').text(v.category);
						template.find('.img-news').css('background-image', "url("+v.enclosure['@attributes'].url+")");
					template.find('.news-wrap').wrapAll('<a href="article?slug='+v.slug+'"></a>');
					wrapper.append(template);
					template.fadeIn('slow');
				});
			});
		};
		start();
	},
};
app.init();
