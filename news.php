<?php 
	$page_category = $_GET['category'];
	include('app/category.php');
	include('partials/header.php');
 ?>
	<!-- Sub Menu -->
	<nav class="sub-menu fixed-top" aria-label="breadcrumb">
      <ol class="oracle-container breadcrumb">
        <li class="breadcrumb-item"><a href="/">News</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?= $page_category; ?></li>
      </ol>
  	</nav>
	<!-- News Block -->
	<div class="oracle-container body-container" id="news_block">
		<div class="row news_header">
			<div class="block-header col-md-12">
				<h2 class="text-center title"><?= ($page_category == 'All') ? 'All News' : $page_category; ?></h2>
			</div>
			<div class="col-md-12">
				<div class="float-right">
					<form class="form-inline">
						<input type="hidden" value="<?= $page_category; ?>" class="page_category" name="page_category">
						<div class="form-group mb-2">
							<select class="form-control form-control-sm border-black select-sort">
								<option value="1">Recent News</option>
								<option value="0">Older News</option>
							</select> 
							<button class="btn btn-sm btn-default btn-black mleft-5 btn-sort" type="button"><i class="fas fa-angle-right"></i></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row news_lists mt-20 ">
			<?php foreach ($page_items as $item) : ?>
				<div class="col-md-4 col-sm-6 news_card">
					<a href="article?slug=<?= $item->slug; ?>">
						<div class="news-wrap">
							<div class="img-news" style="background-image: url(<?= $item->enclosure['url']; ?>)"></div>
							<div class="news-content">
								<p class="category"><i class="fas fa-bullseye"></i> <?= $item->category; ?></p>
								<h5 class="title"><?= $item->title; ?></h5>
							</div>
						</div>
					</a>
				</div>
			<?php endforeach; ?>
		</div>

	</div>

	<div class="col-md-4 col-sm-6 news_card" id="news_card_template">
			<div class="news-wrap">
				<div class="img-news"></div>
				<div class="news-content">
					<p class="category"><i class="fas fa-bullseye"></i> <span class="lblCategory"></span></p>
					<h5 class="title"></h5>
				</div>
			</div>
		<!-- <a href="" class="news-link">
		</a> -->
	</div>


<?php include('partials/footer.php'); ?>
