<?php 
	header('Content-type: application/json');
	include('../app/create_slug.php');
	
	$createSlug = new CreateSlug();
	$url = "https://blogs.oracle.com/rss";
	// $url = "../app/rss.xml";
	$xml = simplexml_load_file($url);

	$sort = $_GET['sort'];
	$page_category = $_GET['category'];

	if($page_category == 'All')
	{
		$page_items = $xml->xpath('channel/item');
	}
	else
	{
		$page_items = $xml->xpath('channel/item[category="'.$page_category.'"]');
	}

	foreach ($page_items as $page_item) {
		$article_slug = $createSlug->slug($page_item->title);
		$page_item->pubDate = date('F d, Y h:i A', strtotime($page_item->pubDate));
		$page_item->slug = $article_slug;
	}


	function date_sort($a, $b)
	{
	    $t1 = strtotime($a['pubDate']);
	    $t2 = strtotime($b['pubDate']);
	    return $t1 - $t2 ;
	} 

	usort($page_items, 'date_sort');

	if($sort == 0)
		$page_items = array_reverse($page_items);

	echo json_encode($page_items);
?>