<?php 
	// namespace App;
	class CreateSlug 
	{
		public function slug($title)
		{
			$slug = iconv('UTF-8', 'ASCII//TRANSLIT', $title); // convert string to requested character encoding
			$slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug); // remove duplicates only letters and numbers
			$slug = strtolower(trim($slug, '-')); // To lowercase and remove unwanted characters on the sides
			$slug = preg_replace("/[\/_|+ -]+/", '-', $slug); // Finally make the clean slug
			return $slug;	
		}
	}


 ?>