<?php 
	
	include('app/create_slug.php');
	
	$createSlug = new CreateSlug();
	$url = "https://blogs.oracle.com/rss";
	// $url = "app/rss.xml";
	$xml = simplexml_load_file($url);
	$categories = [];
	$itemCount = count($xml->channel->item); 
	$items = [];
	$category_items = [];

		for($i = 0; $i < $itemCount; $i++){
		  
			$title = (string) $xml->channel->item[$i]->title;
			$link = $xml->channel->item[$i]->link;
			$description = $xml->channel->item[$i]->description;
			$category = (string) $xml->channel->item[$i]->category;
			$pubDate = $xml->channel->item[$i]->pubDate;
			$enclosure = (string) $xml->channel->item[$i]->enclosure['url'];
			$creator = (string) $xml->channel->item[$i]->children('dc', true)->creator;

			$slug = $createSlug->slug($title);
			
			$item = [
				'title' => $title,
				'link' => $link,
				'slug' => $slug,
				'description' => $description,
				'category' => $category,
				'pubDate' => date('F d, Y h:i A', strtotime($pubDate)),
				'enclosure' => $enclosure,
				'creator' => $creator,
			];

			array_push($items, $item);
			if(!in_array($category, $categories)) {
				array_push($categories, $category);
				array_push($category_items, $item);
			}


			// echo $i;		
			// echo $title;	

			// $html .= "<img src='$enclosure' />"; // Title of post
			// $html .= "<a target='_blank' href='$link'><b>$title</b></a>"; // Title of post
			// $html .= "$description"; // Description
			// $html .= "<br />$pubDate<br /><br />"; // Date Published
		}
	// echo "$html<br />";

			// echo "<pre>";
			// print_r($categories);
			// echo "</pre>";



	// // for filters
	// $items = $xml->xpath('channel/item[category="Finance"]');
	// // var_dump($items);
	// foreach($items as $item) {
 //        echo "Found {$item->category}<br />";
 //    }

		// var_dump($category_items);

?>
