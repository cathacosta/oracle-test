<!doctype html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Oracle</title>
    <!-- <link rel="shortcut icon" href="images/logo.png"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/vendor/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/buttons.css">
    <link rel="stylesheet" href="css/landing.css">

</head>
<body>
	<div id="global_container" class="container-fluid">
		<?php 
            include('app/news.php');
            include('landing/navbar.php'); 
                
        ?>
	           
