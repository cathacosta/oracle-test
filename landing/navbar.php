<header>
  <div class="fixed-top">
    <nav class="oracle-container navbar navbar-expand-lg navbar-dark bg-dark ">
      <a class="" href="/">
        <img src="images/red-black-oracle.gif" class="logo">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link stat" href="/">News</a>
          </li>
        </ul>
        <div class="float-right">
            <ul class="navbar-nav mr-auto ">
              <li class="nav-item active dropdown">
                <a class="nav-link " href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <?php foreach ($categories as $key => $value) : ?>
                    <a class="dropdown-item" href="news?category=<?= $value; ?>"><?= $value; ?></a>
                  <?php endforeach; ?>
                </div>
              </li>
            </ul>
        </div>
        
      </div>
    </nav>
  </div>
  <div class="wrapper">
    <div class="img-div"></div>
    <div class="img-overlay" id="image-overlay"></div>
  </div>
  <!-- <?php //include('landing/carousel.php'); ?> -->
</header>